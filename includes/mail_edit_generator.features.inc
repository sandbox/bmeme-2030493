<?php

/**
 * @file
 * Features integration for Mail Editor Generator module.
 */

/**
 * Implements hook_features_export_options().
 */
function mail_edit_generator_features_export_options() {
  $options = array();

  foreach (mail_edit_generator_templates() as $template) {
    $options[$template] = $template;
  }

  return $options;
}

/**
 * Implements hook_features_export().
 */
function mail_edit_generator_features_export($data, &$export, $module_name = '') {
  $export['dependencies']['features'] = 'features';
  $export['dependencies']['mail_edit'] = 'mail_edit';
  $export['dependencies']['mail_edit'] = 'mail_edit_generator';

  foreach ($data as $component) {
    $export['features']['mail_edit_generator'][$component] = $component;
  }

  return $export;
}

/**
 * Implements hook_features_export_render().
 */
function mail_edit_generator_features_export_render($module, $data) {
  module_load_include('inc', 'mail_edit', 'mail_edit.alter');
  $languages = language_list();

  $code = array();
  $code[] = '$templates = array();';

  foreach ($data as $id => $name) {
    foreach ($languages as $langcode => $language) {
      $mail_template = _mail_edit_load($name, $langcode, TRUE);
      $mtid = !empty($mail_template['id']) ? $mail_template['id'] : $mail_template['type']['id'];

      $code[] = "\n\$templates['{$mtid}']['{$langcode}'] = " . features_var_export($mail_template) . ";";
    }
  }

  $code[] = "return \$templates;";
  $code = implode("\n", $code);

  return array('mail_edit_generator_defaults' => $code);
}

/**
 * Implements hook_features_revert().
 */
function mail_edit_generator_features_revert($module) {
  mail_edit_generator_features_rebuild($module);
}

/**
 * Implements hook_features_rebuild().
 */
function mail_edit_generator_features_rebuild($module) {
  $saved_templates = module_invoke($module, 'mail_edit_generator_defaults');

  foreach ($saved_templates as $id => $template) {
    $saved = mail_edit_generator_save_template($id, $template);
  }
}

function mail_edit_generator_save_template($id, $template) {
  foreach ($template as $lang => $data) {
    $mail_edit_record = $data;
    unset($mail_edit_record['type']);
    $mail_edit_registry_record = $data['type'];

    if (!empty($data['description'])) {
      if (_mail_edit_generator_template_exists($id, $lang)) {
        db_update('mail_edit')
          ->fields(array(
            'description' => $data['description'],
            'subject' => $data['subject'],
            'body' => $data['body']
          ))
          ->condition('id', $id, '=')
          ->condition('language', $lang, '=')
          ->execute();
      }
      else {
        drupal_write_record('mail_edit', $mail_edit_record);
      }

      if (_mail_edit_generator_template_exists($id)) {
        db_update('mail_edit_registry')
          ->fields(array(
            'module' => $data['type']['module'],
            'mailkey' => $data['type']['mailkey'],
            'description' => $data['type']['description'],
            'token_type' => $data['type']['token_type']
          ))
          ->condition('id', $id, '=')
          ->execute();
      }
      else {
        drupal_write_record('mail_edit_registry', $mail_edit_registry_record);
      }
    }
    else {
      if (_mail_edit_generator_template_exists($id, $lang)) {
        db_delete('mail_edit')
          ->condition('id', $id, '=')
          ->condition('language', $lang, '=')
          ->execute();
      }
    }
  }
}

function _mail_edit_generator_template_exists($id, $lang = FALSE) {
  if ($lang) {
    $result = db_select('mail_edit', 'mer')
      ->fields('mer')
      ->condition('mer.id', $id, '=')
      ->condition('mer.language', $lang, '=')
      ->execute()
      ->fetchObject();
  }
  else {
    $result = db_select('mail_edit_registry', 'mer')
      ->fields('mer')
      ->condition('mer.id', $id, '=')
      ->execute()
      ->fetchObject();
  }

  return $result;
}
