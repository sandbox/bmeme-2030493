<?php

/**
 * @file
 * Administrative interface for the Mail Editor Generator module.
 */

/**
 * Form for adding a new Mail Template
 */
function mail_edit_generator_add_form($form, &$form_state) {

  $form['key'] = array(
    '#title' => t('Template key'),
    '#type' => 'textfield',
    '#description' => t('Enter a valid template key without trailing _subject or _body') . '<br>' . t('May only contain lowercase letters, numbers and underscores. <strong>Try to avoid conflicts with the names of existing Mail templates.</strong>'),
    '#element_validate' => array('validate_mail_template_key'),
    '#required' => TRUE
  );

  $form['description'] = array(
    '#title' => t('Template description'),
    '#type' => 'textarea', 
    '#required' => TRUE,
    '#rows' => 2
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Generate')
  );

  return $form;
}

/**
 * Submit Callback
 * @see mail_edit_generator_add_form().
 */
function mail_edit_generator_add_form_submit($form, &$form_state) {
  $list = variable_get('mail_edit_generator_list', array());
  $list[$form_state['values']['key']] = $form_state['values']['description'];
  
  variable_set('mail_edit_generator_list', $list);

  drupal_flush_all_caches();
  drupal_set_message(t('Configurations saved.'));
  
  $form_state['redirect'] = 'admin/config/system/mail-edit';
}

/**
 * Validation callback
 * @see mail_edit_generator_add_form().
 */
function validate_mail_template_key($element, &$form_state) {
  if (!preg_match('!^[a-z0-9_]+$!', $element['#value'])) {
    form_error($element, t('The machine-readable name must contain only lowercase letters, numbers, and underscores.'));
  }

  if (!empty($element['#value']) && in_array($element['#value'], mail_edit_generator_templates())) {
    form_error($element, t('The "@name" must be unique.', array('@name' => $element['title'])));
  }
}

/**
 * Mail Template Settings form
 */
function mail_edit_generator_settings_form($form, &$form_state, $key) {
  global $language;
  module_load_include('inc', 'mail_edit', 'mail_edit.alter');

  $template = _mail_edit_load($key, $language, TRUE);

  $form['key'] = array(
    '#type' => 'item',
    '#markup' => '<b>Template:</b> ' . $key
  );

  $form['id'] = array(
    '#type' => 'hidden',
    '#value' => $key
  );

  $form['description'] = array('#title' => t('Template description'),
    '#type' => 'textarea', 
    '#required' => TRUE, 
    '#rows' => 2,
    '#default_value' => !empty($template['description']) ? $template['description'] : $template['type']['description']
  );

  $tokens = token_info();
  $categories = array_keys($tokens['types']);
  sort($categories);

  $form['token_type'] = array(
    '#title' => t('Provide a token for this template'),
    '#type' => 'select',
    '#options' => array('' => '') + array_combine($categories, $categories),
    '#default_value' => !empty($template['type']['token_type']) ? $template['type']['token_type'] : ''
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update')
  );

  return $form;
}

/**
 * Submit Callback
 * @see mail_edit_generator_add_form().
 */
function mail_edit_generator_settings_form_submit($form, &$form_state) {

  $record = array(
    'description' => $form_state['values']['description'],
    'token_type' => $form_state['values']['token_type']
  );

  if (!$form_state['values']['token_type']) {
    unset($record['token_type']);
  }

  $update = db_update('mail_edit_registry')
    ->fields($record)
    ->condition('id', $form_state['values']['id'], '=')
    ->execute();

  drupal_set_message(t('Configurations saved.'));
  
  $form_state['redirect'] = 'admin/config/system/mail-edit';
}
